# Android Engineer Coding Challenge

## Introduction

Thank you for applying for the Android Engineer position at Quandoo. In order to have a good understanding of your skill level, we would like to proceed with a technical assessment.

## Task description
It's a really common standard that a consumer facing application fetch its data through a REST API.
Let's build a merchant browser, which fetches data from Quandoo's test public API.
The app should consist of a simple navigation-based restaurant browser and restaurant detail view.

Please access our documentation to get to know the endpoints and response format
https://docs.quandoo.com/interactive-api/ 

### Description

On launch your app should fetch the first 120 merchants (with a 30 items/page limit)
When the user taps on the merchant , it should lead to a merchant detail page.


### Required

Build a simple Android native app that provides a grid with 3 columns.
  - Your cell must show the first restaurant image and name only
  - Your detail page must show: restaurant images, name, rating, address and any other information that you might find useful.
  - Ensure that your scroll experience is smooth and does not show messy results.
  - The usage of KOTLIN and MVVM are mandatory.
  - **Show us what you got**: Focus on a **good architecture**, **separation of concerns** and **test coverage**. 
  - A good and comprehensive readme file is also welcome.
  - The usage of third parties is permitted and will be taken into account while reviewing your code.
  - Make sure your project compiles and it is properly setup. 
  - Share your solution via Gitlab with us.

### Optional

  - Make the list endless fetching items on demand when the user scrolls.
  - Implement an offline mode which stores the items on disk, so the app is still usable with no internet.
  - Implement a Landscape mode

### Tips
  - Design is not a priority but we are going to definitely look how you define your layouts and feed them with data.
  - merchants = restaurants.
  - Your app must be ready to support different phone sizes.


[Give us your feedback about this task.](https://docs.google.com/forms/d/1QfYEJRDgtGOtdP3Tejrrr7wezGLHuejBgo6bauGSdl0/viewform?edit_requested=true)

